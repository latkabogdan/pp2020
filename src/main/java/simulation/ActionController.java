package simulation;

import simulation.model.Cell;

import java.util.Map;

public interface ActionController {
    Map<Long, Cell> makeAction(Map<Long, Cell> cellsById);

    Cell mergeCells(Cell cell, Cell otherCell);

    Metrics getMetrics(GraphPart graphPart);
}
