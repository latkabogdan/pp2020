package simulation;

import java.util.List;
import java.util.Random;
import java.util.Set;

public interface Direction {

    Set<Direction> getNeighbours();

    Direction getOpposite();

    List<Direction> getAll();

    default Direction getRandom() {
        Random rand = new Random();
        return getAll().get(rand.nextInt(getAll().size()));
    }

    @Override
    String toString();
}
