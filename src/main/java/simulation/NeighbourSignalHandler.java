package simulation;

import simulation.model.Cell;

import java.util.List;

public interface NeighbourSignalHandler {
    List<Signal> getSignalsToPropagate(Cell cell);
}
