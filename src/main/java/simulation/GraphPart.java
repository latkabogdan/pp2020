package simulation;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.Logger;
import simulation.model.BlockedCell;
import simulation.model.Cell;
import simulation.parallel.NeighbourBuffer;
import simulation.parallel.message.BufferMessage;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@Setter
public class GraphPart {
    private final NeighbourSignalHandler neighbourSignalHandler;
    private Map<Long, Cell> cells;
    private Map<Long, Cell> newCells;
    private List<NeighbourBuffer> neighbourBuffers;
    private Map<Long, Cell> incomingNeighbours;
    private Logger logger;
    private final ActionController actionController;

    public void runSignalPropagation(ActorRef self) {
        cells.values()
                .forEach(cell -> propagateSignal(self, cell));
        neighbourBuffers.stream()
                .filter(NeighbourBuffer::getIsModified)
                .forEach(buffer -> sendBuffer(self, buffer));
        cells = newCells;
    }

    private void propagateSignal(ActorRef self, Cell cell) {
        List<Signal> signalsToPropagate = neighbourSignalHandler.getSignalsToPropagate(cell);
        signalsToPropagate.forEach(signal -> {
            Direction direction = signal.getDirection();
            Long id = cell.getNeighboursByDirection().get(direction);
            if (id == null) return;
            if (cells.containsKey(id)) {
                if(cells.get(id).isBlocked()) {
                    return;
                }
                newCells.get(id).addSignal(signal);
            } else {
                neighbourBuffers.stream()
                        .filter(buffer -> buffer.containsCell(id))
                        .findFirst()
                        .ifPresentOrElse(buffer -> buffer.addSignal(id, signal), () -> logger.error("Missing cell with id {} in Actor {}", id, self));
            }
        });
        cell.clearSignals();
    }

    private void sendBuffer(ActorRef self, NeighbourBuffer buffer) {
        NeighbourBuffer copy = new NeighbourBuffer(buffer);
        copy.sendBuffer(self);
        buffer.clearBuffer();
    }

    public void mergeNeighbours() {
        incomingNeighbours.values()
                .stream()
                .filter(cell -> !cell.isBlocked())
                .forEach(cell -> {
                    //Pair<Cell, Cell> merge = cells.get(cell.getId()).merge(cell);
                    //handleMove(merge.getRight());
                    cells.computeIfPresent(cell.getId(), (k,v) -> actionController.mergeCells(v, cell));
                    cells.get(cell.getId()).mergeSignals(cell);
                });
        incomingNeighbours.clear();
    }

    public Metrics makeMoves() {
        /*cells.values()
                .stream()
                .filter(cell -> !(cell instanceof BlockedCell))
                .forEach(this::makeAction);*/
        this.cells = actionController.makeAction(this.cells);
        newCells = cells.values()
                .stream()
                .map(Cell::getClearCopy)
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
        return actionController.getMetrics(this);
    }

    private void makeAction(Cell cell) {
        if(cell instanceof BlockedCell) {
            return;
        }
        /*Direction moveDirection = cell.getMoveDirection().orElseGet(() -> getRandomDirection(cell));
        Map<Direction, Long> neighboursByDirection = cell.getNeighboursByDirection();
        Long targetId = neighboursByDirection.get(moveDirection);
        Cell secondCell = Optional.ofNullable(cells.get(targetId)).orElse(cell);
        Pair<Cell, Cell> firstToSecond = cell.merge(secondCell);
        handleMove(firstToSecond.getLeft());
        handleMove(firstToSecond.getRight());*/
        this.cells = actionController.makeAction(this.cells);
        //Optional<Cell> newCell = cellsToSave.stream().findFirst();
        //newCell.ifPresent(c -> cells.replace(c.getId(), c));
        //newCell.ifPresent(cellsToSave::remove);
        //cellsToSave.forEach(cell1 -> cells.computeIfPresent(cell1.getId(), (k,v) -> actionController.mergeCells(v, cell1)));
    }

    private void handleMove(Cell cell) {
        if(cells.containsKey(cell.getId())) {
            newCells.replace(cell.getId(), cell);
        } else {
            neighbourBuffers.stream()
                    .filter(buffer -> buffer.containsCell(cell.getId()))
                    .findFirst()
                    .ifPresentOrElse(buffer -> buffer.replaceWithUnitCell(cell), () -> logger.error("Missing cell with id {}", cell.getId()));
        }
    }



    public boolean containsCell(Long id) {
        return cells.containsKey(id);
    }

    public void handleBuffer(BufferMessage buffer) {
        Map<Long, Cell> cellById = buffer.getCellById();
        incomingNeighbours.putAll(cellById);
    }
}
