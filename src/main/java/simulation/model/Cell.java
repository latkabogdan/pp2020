package simulation.model;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;
import simulation.Direction;
import simulation.Signal;

import java.util.*;

@Getter
@Setter
public abstract class Cell {
    private final Long id;
    private final Map<Direction, Long> neighboursByDirection;
    private List<Signal> signals;

    public Cell(Long id) {
        this.id = id;
        this.neighboursByDirection = Maps.newHashMap();
        this.signals = Lists.newLinkedList();
    }

    public Cell(Cell cell) {
        this.id = cell.id;
        this.neighboursByDirection = new HashMap<>(cell.neighboursByDirection);
        this.signals = new LinkedList<>(cell.signals);
    }

    public Cell(Long id, Map<Direction, Long> neighboursByDirection) {
        this.id = id;
        this.neighboursByDirection = neighboursByDirection;
        this.signals = Lists.newLinkedList();
    }

    public List<Signal> getSignals() {
        return signals;
    }

    public void addNeighbours(Map<Direction, Long> neighbours) {
        this.neighboursByDirection.putAll(neighbours);
    }

    public void addNeighbour(Direction direction, Long neighbour) {
        this.neighboursByDirection.put(direction, neighbour);
    }

    public void addSignal(Signal signal) {
        this.signals.add(signal);
    }

    public Long getId(){
        return this.id;
    }

    public Optional<Direction> getMoveDirection() {
        return signals.stream()
                .filter(signal -> signal.getValue() > 0.2)
                .max(Comparator.comparingDouble(Signal::getValue))
                .map(Signal::getDirection)
                .map(Direction::getOpposite);
    }

    public List<Direction> getNeighbourDirections() {
        return new ArrayList<>(neighboursByDirection.keySet());
    }

    public Long getNeighbour(Direction direction) {
        return neighboursByDirection.get(direction);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return getId().equals(cell.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public void clearSignals() {
        this.signals = Lists.newLinkedList();
    }

    public void clear() {
        clearSignals();
    }

    public void mergeSignals(Cell cell) {
        List<Signal> comingSignals = cell.getSignals();
        this.signals.addAll(comingSignals);
    }

    public void removeNeighbour(Long id) {
        Optional<Direction> direction = neighboursByDirection.entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals(id))
                .map(Map.Entry::getKey)
                .findFirst();
        direction.ifPresent(neighboursByDirection::remove);
    }

    public boolean isBlocked() {
        return this instanceof BlockedCell;
    }

    public abstract Cell getCopy();

    public abstract Cell getClearCopy();

    public Pair<Cell, Cell> merge(Cell secondCell) {
        return Pair.of(this, secondCell);
    }
}
