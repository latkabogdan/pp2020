package simulation.model;

import simulation.Direction;

import java.util.Map;

public class BlockedCell extends Cell{

    public BlockedCell(Long id) {
        super(id);
    }

    public BlockedCell(Cell cell) {
        super(cell);
    }

    public BlockedCell(Long id, Map<Direction, Long> neighboursByDirection) {
        super(id, neighboursByDirection);
    }

    @Override
    public Cell getCopy() {
        return new BlockedCell(this);
    }

    @Override
    public Cell getClearCopy() {
        return new BlockedCell(this.getId(), this.getNeighboursByDirection());
    }
}
