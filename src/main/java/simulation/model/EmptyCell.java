package simulation.model;

import simulation.Direction;

import java.util.Map;

public class EmptyCell extends Cell{
    public EmptyCell(Long id) {
        super(id);
    }

    public EmptyCell(Cell cell) {
        super(cell);
    }

    public EmptyCell(Long id, Map<Direction, Long> neighboursByDirection) {
        super(id, neighboursByDirection);
    }

    @Override
    public Cell getCopy() {
        return new EmptyCell(this);
    }

    @Override
    public Cell getClearCopy() {
        return new EmptyCell(this.getId(), this.getNeighboursByDirection());
    }
}
