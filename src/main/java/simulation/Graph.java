package simulation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import configuration.Configuration;
import configuration.Parameters;
import foraminifiera.directionImpl.ClassicSignalHandler;
import foraminifiera.model.ForaminiferaActionController;
import org.apache.logging.log4j.LogManager;
import simulation.model.Cell;
import simulation.parallel.NeighbourBuffer;
import simulation.parallel.SimulationActor;
import simulation.parallel.ViewActor;
import simulation.parallel.message.InitMessage;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Graph {
    private ActorSystem system;
    protected List<Cell> cells;
    private ActorRef viewActor;

    public Graph(Configuration configuration) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Class<? extends GraphSupplier> graphSupplier = configuration.getGraphSupplier();
        this.cells = graphSupplier.getDeclaredConstructor()
                .newInstance()
                .getGraph();
    }

    public void run() {
        system = ActorSystem.create("simulation");
        viewActor = getViewActor();
        List<GraphPart> graphParts = splitCellsForActors(Parameters.NUMBER_OF_ACTORS);
        Map<GraphPart, ActorRef> simulationActors = getActorsByGraphPart(graphParts);
        LongStream.range(0, simulationActors.size())
                .forEach(id -> initActor(graphParts, simulationActors, id));
    }

    private ActorRef getViewActor() {
        return system.actorOf(ViewActor.props(cells), "viewActor");
    }

    private List<GraphPart> splitCellsForActors(int numberOfActors) {
        return Lists.partition(cells, cells.size() / numberOfActors)
                .stream()
                .map(this::mapToGraphPart)
                .collect(Collectors.toList());
    }

    private GraphPart mapToGraphPart(List<Cell> cells) {
        Map<Long, Cell> cellsByIds = cells.stream()
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
        return new GraphPart(new ClassicSignalHandler(), cellsByIds, new HashMap<>(), new LinkedList<>(), new HashMap<>(), LogManager.getLogger("file"), new ForaminiferaActionController());
    }

    private Map<GraphPart, ActorRef> getActorsByGraphPart(List<GraphPart> graphParts) {
        return IntStream.range(0, graphParts.size())
                .mapToObj(number -> Maps.immutableEntry(graphParts.get(number), getActor(graphParts.get(number), number)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private ActorRef getActor(GraphPart graphPart, int number) {
        return system.actorOf(SimulationActor.props(graphPart), "simulationActor" + number);
    }

    private void initActor(List<GraphPart> graphParts, Map<GraphPart, ActorRef> simulationActors, long id) {
        ActorRef actorRef = extractActorByIndex(simulationActors, id);
        InitMessage msg = new InitMessage(id, viewActor, system, getNeighboursBuffers(graphParts.get((int) id), simulationActors));
        actorRef.tell(msg, null);
    }

    private ActorRef extractActorByIndex(Map<GraphPart, ActorRef> simulationActors, long id) {
        return new ArrayList<>(simulationActors.values()).get((int) id);
    }

    private List<NeighbourBuffer> getNeighboursBuffers(GraphPart graphPart, Map<GraphPart, ActorRef> simulationActors) {
        Map<Long, Cell> cellsByIdsCopy = getCellByIdsCopy();
        Map<ActorRef, List<Long>> actorsToNeighbours = getNeighboursForActor(graphPart, simulationActors);
        return actorsToNeighbours.entrySet()
                .stream()
                .map(actorToNeighbours -> buildNeighbourBuffer(cellsByIdsCopy, actorToNeighbours))
                .collect(Collectors.toList());
    }

    private Map<Long, Cell> getCellByIdsCopy() {
        return cells.stream()
                .map(Cell::getCopy)
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
    }

    private Map<ActorRef, List<Long>> getNeighboursForActor(GraphPart graphPart, Map<GraphPart, ActorRef> simulationActors) {
        Map<Long, Cell> partCells = graphPart.getCells();
        List<Long> outerNeighbourIds = extractOuterNeighbours(graphPart, partCells);
        return outerNeighbourIds.stream()
                .filter(id -> getActorForNeighbour(simulationActors, id).isPresent())
                .collect(Collectors.groupingBy(id -> getActorForNeighbour(simulationActors, id).get()));
    }

    private List<Long> extractOuterNeighbours(GraphPart graphPart, Map<Long, Cell> partCells) {
        return partCells.values()
                .stream()
                .map(Cell::getNeighboursByDirection)
                .map(Map::entrySet)
                .flatMap(Set::stream)
                .map(Map.Entry::getValue)
                .filter(id -> !graphPart.containsCell(id))
                .collect(Collectors.toList());
    }

    private Optional<ActorRef> getActorForNeighbour(Map<GraphPart, ActorRef> simulationActors, Long id) {
        return simulationActors.entrySet()
                .stream()
                .filter(partToActor -> partToActor.getKey().containsCell(id))
                .findFirst()
                .map(Map.Entry::getValue);
    }

    private NeighbourBuffer buildNeighbourBuffer(Map<Long, Cell> cellsByIdsCopy, Map.Entry<ActorRef, List<Long>> actorToNeighbours) {
        Map<Long, Cell> cellByIdForNeighbour = cellsByIdsCopy.entrySet()
                .stream()
                .filter(cellById -> containsCell(actorToNeighbours, cellById))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return new NeighbourBuffer(cellByIdForNeighbour, actorToNeighbours.getKey(), Boolean.FALSE);
    }

    private boolean containsCell(Map.Entry<ActorRef, List<Long>> actorToNeighbours, Map.Entry<Long, Cell> cellById) {
        return actorToNeighbours.getValue().contains(cellById.getKey());
    }
}
