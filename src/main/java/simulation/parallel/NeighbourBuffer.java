package simulation.parallel;

import akka.actor.ActorRef;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import simulation.model.Cell;
import simulation.Signal;
import simulation.model.EmptyCell;
import simulation.parallel.message.BufferMessage;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class NeighbourBuffer {
    private Map<Long, Cell> cellById;
    private ActorRef neighbour;
    private Boolean isModified;

    public NeighbourBuffer(NeighbourBuffer buffer) {
        this.cellById = buffer.getCellById().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getCopy()));
        this.neighbour = buffer.getNeighbour();
        this.isModified = buffer.getIsModified();
    }

    public boolean containsCell(Long id) {
        return cellById.containsKey(id);
    }

    public void addSignal(Long id, Signal signal) {
        isModified = Boolean.TRUE;
        cellById.get(id).addSignal(signal);
    }

    public void replaceWithUnitCell(Cell cell) {
        isModified = Boolean.TRUE;
        cellById.replace(cell.getId(), cell);
    }

    public void clearBuffer() {
        isModified = Boolean.FALSE;
        cellById = cellById.values()
                .stream()
                .map(EmptyCell::new)
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
    }

    public void sendBuffer(ActorRef sender) {
        BufferMessage bufferMessage = new BufferMessage(cellById);
        neighbour.tell(bufferMessage, sender);
    }
}
