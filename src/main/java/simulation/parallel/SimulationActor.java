package simulation.parallel;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import configuration.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simulation.GraphPart;
import simulation.Metrics;
import simulation.parallel.message.*;

import java.time.Duration;

public class SimulationActor extends AbstractActor {

    private final GraphPart graphPart;
    private Long id;
    private ActorRef viewActor;
    private Logger logger;
    private ActorSystem system;

    SimulationActor(GraphPart graphPart) {
        this.graphPart = graphPart;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(InitMessage.class, this::init)
                .match(IterationMessage.class, this::runIteration)
                .match(FinishIterationMessage.class, this::finishIteration)
                .match(BufferMessage.class, this::handleBuffer)
                .build();
    }

    private void handleBuffer(BufferMessage buffer) {
        graphPart.handleBuffer(buffer);
    }

    private void finishIteration(FinishIterationMessage message) throws InterruptedException {
        long iteration = message.getIteration();
        viewActor.tell(new UpdateStateMessage(graphPart.getCells().values()), self());
        if (iteration < Parameters.MAX_ITERATIONS) {
            system.scheduler().scheduleOnce(
                    Duration.ofMillis(10), self(), new IterationMessage(iteration + 1), system.dispatcher(), self());       //setting time
            //self().tell(new IterationMessage(iteration + 1), self());
        } else {
            Thread.sleep(10000);
            context().system().terminate();
        }
    }

    private void init(InitMessage message) {
        logger = LogManager.getLogger("file");
        this.id = message.getId();
        this.viewActor = message.getViewActor();
        this.system = message.getSystem();
        this.graphPart.setNeighbourBuffers(message.getNeighbourBuffers());
        //logger.info("Initialization actor {} succeed", id);
        self().tell(new IterationMessage(1L), self());
    }

    private void runIteration(IterationMessage iterationMessage) {
        graphPart.mergeNeighbours();
        Metrics metrics = graphPart.makeMoves();
        logger.info(id + ", " + iterationMessage.getIteration() + ", " + metrics.toString());
        graphPart.runSignalPropagation(self());
        Long iteration = iterationMessage.getIteration();
        try {       //TODO to remove
            Thread.sleep(100);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
        self().tell(new FinishIterationMessage(iteration), self());
    }

    public static Props props(GraphPart graphPart) {
        return Props.create(SimulationActor.class, graphPart);
    }
}
