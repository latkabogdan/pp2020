package simulation.parallel.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import simulation.model.Cell;

import java.util.Collection;

@AllArgsConstructor
@Getter
public class UpdateStateMessage {
    private final Collection<Cell> cells;
}
