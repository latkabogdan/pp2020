package simulation.parallel.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FinishIterationMessage {
    private final long iteration;
}
