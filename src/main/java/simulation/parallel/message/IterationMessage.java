package simulation.parallel.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class IterationMessage {
    private final Long iteration;
}
