package simulation.parallel.message;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import simulation.parallel.NeighbourBuffer;

import java.util.List;

@AllArgsConstructor
@Getter
public class InitMessage {
    private final Long id;
    private final ActorRef viewActor;
    private final ActorSystem system;
    private final List<NeighbourBuffer> neighbourBuffers;
}
