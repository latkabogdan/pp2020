package simulation.parallel.message;

import lombok.AllArgsConstructor;
import lombok.Getter;
import simulation.model.Cell;

import java.util.Map;

@AllArgsConstructor
@Getter
public class BufferMessage {
    private final Map<Long, Cell> cellById;
}
