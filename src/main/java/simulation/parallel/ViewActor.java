package simulation.parallel;

import akka.actor.AbstractActor;
import akka.actor.Props;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import simulation.model.BlockedCell;
import simulation.model.Cell;
import foraminifiera.model.ForaminiferaCell;
import simulation.parallel.message.UpdateStateMessage;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ViewActor extends AbstractActor {

    private final Logger logger;
    private final Graph graph;
    private final Map<Long, Node> nodes;
    private Viewer viewer;
    private static final Map<Predicate<Object>, String> CELL_TYPE_TO_COLOR = Map.of(
            BlockedCell.class::isInstance, "grey",
            ForaminiferaCell.class::isInstance, "green"
    );

    ViewActor(Map<Long, Cell> cells) {
        this.logger = LogManager.getLogger("ViewLogger");
        //logger.info("ViewActor started");
        graph = new SingleGraph("abc");
        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        graph.setAttribute("ui.stylesheet", "edge { visibility-mode: hidden}");
        graph.setStrict(false);
        graph.setAutoCreate(true);
        System.setProperty("org.graphstream.ui", "swing");
        graph.setAttribute("ui.stylesheet", "node { size: 3px;}");
        graph.setAttribute("ui.stylesheet", "node.empty { fill-color: black; size: 3px;}");
        graph.setAttribute("ui.stylesheet", "node.entity { fill-color: red; size: 3px;}");
        graph.setAttribute("ui.stylesheet", "node.signal { fill-color: yellow; size: 3px;}");
        nodes = cells.values()
                .stream()
                .collect(Collectors.toMap(Cell::getId, this::buildNode));
        cells.values()
                .forEach(cell -> {
                    Collection<Long> neighbours = cell.getNeighboursByDirection().values();
                    neighbours.stream()
                            .filter(neighbour -> graph.getEdge("neighbour" + neighbour.toString() + nodes.get(cell.getId())) == null && graph.getEdge("neighbour" + nodes.get(cell.getId()) + neighbour.toString()) == null)
                            .forEach(neighbour -> graph.addEdge("neighbour" + neighbour.toString() + nodes.get(cell.getId()), nodes.get(neighbour), nodes.get(cell.getId())));
                });
        graph.edges().forEach(edge -> edge.setAttribute("ui.hide"));
        viewer = graph.display();
        viewer.getDefaultView().enableMouseOptions();
    }

    private Node buildNode(Cell cell) {
        Node node = graph.addNode(cell.getId().toString());
        node.setAttribute("ui.style", "size: 3px;");
        Optional<String> color = getCorrectColor(cell);
        node.setAttribute("ui.style", "fill-color: " + color.orElse("black") + ";");
        return node;
    }

    private Optional<String> getCorrectColor(Cell cell) {
        if (cell.isBlocked()) {
            return Optional.of("grey");
        }
        double colorValue = ((ForaminiferaCell) cell).getAlgae() * 10000 / 255;
        return Optional.of("rgb(" + (int) colorValue % 255 + ",255,255)");
    }

    @Override
    public Receive createReceive() {
        //logger.info("ViewActor received message");
        return receiveBuilder()
                .match(UpdateStateMessage.class, this::updateState)
                .build();
    }

    private void updateState(UpdateStateMessage message) {
        message.getCells().forEach(cell -> {
            Optional<String> color = getCorrectColor(cell);
            //nodes.get(cell.getId()).setAttribute("ui.style", "fill-color: " + color.orElse(cell.getSignals().isEmpty() ? "black" : "yellow") + ";");
            nodes.get(cell.getId()).setAttribute("ui.style", "fill-color: " + color.orElse(cell.getSignals().isEmpty() ? "black" : "yellow") + ";");
        });
    }

    public static Props props(List<Cell> cells) {
        Map<Long, Cell> cellsByIds = cells.stream()
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
        return Props.create(ViewActor.class, cellsByIds);
    }
}
