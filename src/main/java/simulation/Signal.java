package simulation;

import java.util.Objects;

public class Signal {
    private final Direction direction;
    private final Double value;

    public Signal(Direction direction, Double value){
        this.direction = direction;
        this.value = value;
    }

    public Signal(Signal signal) {
        this.direction = signal.getDirection();
        this.value = signal.getValue();
    }

    public Direction getDirection(){
        return this.direction;
    }

    public Double getValue(){
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Signal signal = (Signal) o;
        return getDirection().equals(signal.getDirection());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDirection());
    }

    public static Signal merge(Signal signal, Signal opposite) {
        if(signal.getDirection().getOpposite() == opposite.getDirection()) {
            double total = signal.getValue() - opposite.getValue();
            Direction direction = total > 0 ? signal.getDirection() : opposite.getDirection();
            return new Signal(direction, total);
        }
        else {
            return signal;
        }
    }
}
