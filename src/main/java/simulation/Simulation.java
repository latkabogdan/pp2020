package simulation;

import configuration.Configuration;
import foraminifiera.directionImpl.BaseDirection;
import foraminifiera.directionImpl.ClassicSignalHandler;
import foraminifiera.graphfactory.SeaBottomFactory;

public class Simulation {
    protected Graph graph;

    public Simulation(){
        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        Configuration configuration = new Configuration(BaseDirection.class, ClassicSignalHandler.class, SeaBottomFactory.class);
        try {
            graph = new Graph(configuration);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        graph.run();
    }
}
