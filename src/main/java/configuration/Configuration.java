package configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import simulation.Direction;
import simulation.GraphSupplier;
import simulation.NeighbourSignalHandler;

@Getter
@AllArgsConstructor
public class Configuration {
    private final Class<? extends Direction> direction;
    private final Class<? extends NeighbourSignalHandler> neighbourSignalHandler;
    private final Class<? extends GraphSupplier> graphSupplier;
}
