package configuration;

public class Parameters {

    public static final Long WIDTH = 25L;
    public static final Long HEIGHT = 15L;
    public static final Long DEPTH = 8L;
    public static final Double NEIGHBOUR_MULTIPLIER = 0.3;
    public static final Double SUPPRESSION_FACTOR = 0.6;
    public static final Long MAX_ITERATIONS = 2000L;
    public static final Integer NUMBER_OF_ACTORS = 1;
    public static final Double ATTENUATION_FACTOR = 0.4;
    public static final Double FORAMINIFERA_SPAWN_CHANCE = 0.9;
    public static final Integer FORAMINIFERA_MAX_START_SPAWN = 2;
    public static final Double FORAMINIFERA_REPRODUCTION_THRESHOLD = 0.8;
    public static final Double FORAMINIFERA_REPRODUCTION_COST = 0.7;
    public static final Double ALGAE_ENERGETIC_CAPACITY = 0.3;
    public static final Double FORAMINIFERA_LIFE_ACTIVITY_COST = 0.2;
    public static final Double ALGAE_REGENERATION_RATE = 0.05;
    public static final Double FORAMINIFERA_START_ENERGY = 0.3;
    public static final Double ALGAE_MAX_START_VALUE = 0.5;
    public static final Integer CHAMBERS_TO_REPRODUCE = 4;

    private Parameters() {
    }

}
