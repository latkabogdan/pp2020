package foraminifiera;

import lombok.AllArgsConstructor;
import lombok.Getter;
import simulation.Metrics;

@AllArgsConstructor
@Getter
public class ForaminiferaMetrics extends Metrics {
    private final Double algaeCount;
    private final Long foraminiferaCount;

    @Override
    public String toString() {
        return algaeCount + ", " + foraminiferaCount;
    }
}
