package foraminifiera.model;

import com.google.common.collect.Maps;
import configuration.Parameters;
import foraminifiera.ForaminiferaMetrics;
import simulation.ActionController;
import simulation.Direction;
import simulation.GraphPart;
import simulation.Metrics;
import simulation.model.BlockedCell;
import simulation.model.Cell;

import java.util.*;
import java.util.stream.Collectors;

public class ForaminiferaActionController implements ActionController {

    public Map<Long, Cell> makeAction(Map<Long, Cell> cellsById) {
        cellsById.values()
                .stream()
                .filter(cell -> !(cell instanceof BlockedCell))
                .forEach(cell -> makeAction(cell, cellsById));
        return cellsById;
    }

    public void makeAction(Cell cell, Map<Long, Cell> cellsById) {        //czy słusznie założyłem że tylko jedna akcja??
        ForaminiferaCell foraminiferaCell = (ForaminiferaCell) cell;
        foraminiferaCell.algaeGrowth();     //rozrost alg
        Map<Long, ForaminiferaCell> foraminiferaCellsById = Maps.transformValues(Maps.filterValues(cellsById, ForaminiferaCell.class::isInstance), ForaminiferaCell.class::cast);
        List<Foraminifera> foraminiferas = foraminiferaCell.getForaminifera();
        List<Foraminifera> foraminiferasToRemove = new LinkedList<>();
        List<Foraminifera> foraminiferasToAdd = new LinkedList<>();
        //<Cell> modifiedCellsToReturn = new LinkedList<>();
        for(Foraminifera foraminifera : foraminiferas) {
            if(foraminifera.isAbleToReproduce()) {      //rozmnazanie
                foraminifera.reduceEnergy(Parameters.FORAMINIFERA_REPRODUCTION_COST);
                foraminiferasToAdd.add(new Foraminifera(Parameters.FORAMINIFERA_START_ENERGY));
            } else if(foraminiferaCell.isEnoughAlgaeToEat()) {      //jedzenie
                foraminifera.increaseEnergy();
                foraminiferaCell.eatAlgae();
            } else if(foraminifera.shouldDie()) {       //umieranie
                foraminiferasToRemove.add(foraminifera);
            } else {        //poruszanie
                foraminiferasToRemove.add(foraminifera);
                foraminifera.reduceEnergy(Parameters.FORAMINIFERA_LIFE_ACTIVITY_COST);
                makeMove(foraminifera, foraminiferaCell, foraminiferaCellsById);
            }
        }
        foraminiferaCell.removeForaminifera(foraminiferasToRemove);     //do poprawy
        foraminiferaCell.addForaminiferas(foraminiferasToAdd);
        //foraminiferaToAdd.ifPresent(foraminiferaCell::addForaminifera);
        //return modifiedCellsToReturn;
    }

    public void makeMove(Foraminifera foraminifera, ForaminiferaCell cell, Map<Long, ForaminiferaCell> cellsById) {
        Direction moveDirection = cell.getMoveDirection().orElseGet(() -> getRandomDirection(cell));
        Map<Direction, Long> neighboursByDirection = cell.getNeighboursByDirection();
        Long targetId = neighboursByDirection.get(moveDirection);
        Optional<ForaminiferaCell> targetCell = Optional.ofNullable(cellsById.get(targetId)).or(() -> getRandomNeighbour(moveDirection, cell, cellsById));
        targetCell.ifPresent(t -> t.addForaminifera(foraminifera));
        /*LinkedList<Cell> foraminiferaCells = new LinkedList<>();
        foraminiferaCells.add(cell);
        targetCell.ifPresent(foraminiferaCells::add);       //do poprawy
        return foraminiferaCells;*/
    }

    private Optional<ForaminiferaCell> getRandomNeighbour(Direction moveDirection, ForaminiferaCell cell, Map<Long, ForaminiferaCell> cellsById) {
        List<Direction> neighbours = new LinkedList<>(moveDirection.getNeighbours());
        List<ForaminiferaCell> existingNeighbours = neighbours.stream()
                .map(c -> cell.getNeighboursByDirection().get(c))
                .map(cellsById::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return existingNeighbours.stream().findAny();
    }

    private Direction getRandomDirection(Cell cell) {
        Random random = new Random();
        List<Direction> neighbourDirections = cell.getNeighbourDirections();
        return neighbourDirections.get(random.nextInt(neighbourDirections.size()));
    }

    @Override
    public Cell mergeCells(Cell cell, Cell otherCell) {
        return cell.getCopy();
    }

    @Override
    public Metrics getMetrics(GraphPart graphPart) {
        long foraminiferaCount = 0L;
        Double algaeCount = 0.0;
        for(Cell cell: graphPart.getCells().values()) {
            if(cell instanceof BlockedCell) {
                continue;
            }
            ForaminiferaCell foraminiferaCell = (ForaminiferaCell) cell;
            foraminiferaCount += foraminiferaCell.getForaminifera().size();
            algaeCount += foraminiferaCell.getAlgae();
        }
        return new ForaminiferaMetrics(algaeCount, foraminiferaCount);
    }
}
