package foraminifiera.model;

import configuration.Parameters;
import lombok.Getter;
import lombok.Setter;
import simulation.Direction;
import simulation.Signal;
import simulation.model.Cell;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@Setter
public class ForaminiferaCell extends Cell {        //sprawdzić co trzeba przeciążyć
    private List<Foraminifera> foraminifera;
    private Double algae;

    public ForaminiferaCell(Long id) {
        super(id);
        this.foraminifera = new LinkedList<>();
        this.algae = Parameters.ALGAE_MAX_START_VALUE;
    }

    public ForaminiferaCell(ForaminiferaCell cell) {
        super(cell);
        this.foraminifera = cell.getForaminifera();
        this.algae = cell.getAlgae();
    }

    public ForaminiferaCell(Cell cell) {
        super(cell);
        this.foraminifera = new LinkedList<>();
        this.algae = Parameters.ALGAE_MAX_START_VALUE;
    }

    public ForaminiferaCell(Long id, Map<Direction, Long> neighboursByDirection, List<Foraminifera> foraminifera, Double algae) {
        super(id, neighboursByDirection);
        this.foraminifera = foraminifera;
        this.algae = algae;
    }

    public ForaminiferaCell(Long id, Map<Direction, Long> neighboursByDirection, Long value) {
        super(id, neighboursByDirection);
        this.foraminifera = new LinkedList<>();
        this.algae = Parameters.ALGAE_MAX_START_VALUE;
    }

    @Override
    public Cell getCopy() {
        return new ForaminiferaCell(this);
    }

    @Override
    public Cell getClearCopy() {
        return new ForaminiferaCell(this.getId(), this.getNeighboursByDirection(), this.getForaminifera(), this.getAlgae());
    }

    public Map<Direction, Signal> propagateSignals() {
        List<Direction> neighbourDirections = getNeighbourDirections();
        return neighbourDirections
                .stream()
                .collect(Collectors.toMap(Function.identity(), direction -> new Signal(direction, algae)));
    }

    public void addForaminifera(Double energy) {
        this.foraminifera.add(new Foraminifera(energy));
    }

    public void algaeGrowth() {     //TODO
        this.algae *= (1+Parameters.ALGAE_REGENERATION_RATE);
    }

    public boolean isEnoughAlgaeToEat() {
        return algae > Parameters.ALGAE_ENERGETIC_CAPACITY;
    }

    public void eatAlgae() {
        this.algae -= Parameters.ALGAE_ENERGETIC_CAPACITY;
    }

    public void addForaminifera(Foraminifera newForaminifera) {
        foraminifera.add(newForaminifera);
    }

    public void addForaminiferas(Collection<Foraminifera> foraminiferas) {
        foraminifera.addAll(foraminiferas);
    }

    public void removeForaminifera(List<Foraminifera> foraminiferasToRemove) {
        foraminifera.removeAll(foraminiferasToRemove);
    }
}
