package foraminifiera.model;

import configuration.Parameters;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Foraminifera {
    private Double energy;

    public boolean isAbleToReproduce() {
        return this.energy > Parameters.FORAMINIFERA_REPRODUCTION_THRESHOLD;
    }

    public void reduceEnergy(Double foraminiferaReproductionCost) {
        this.energy -= foraminiferaReproductionCost;
    }

    public void increaseEnergy() {
        this.energy += Parameters.ALGAE_ENERGETIC_CAPACITY;
    }

    public boolean shouldDie() {
        return this.energy < Parameters.FORAMINIFERA_LIFE_ACTIVITY_COST;
    }
}
