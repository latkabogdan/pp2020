package foraminifiera.directionImpl;

import com.google.common.collect.ImmutableMap;
import simulation.Direction;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public enum BaseDirection implements Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST,
    UP,
    DOWN,
    NORTHEAST,
    NORTHWEST,
    SOUTHEAST,
    SOUTHWEST,
    UPNORTH,
    UPEAST,
    UPWEST,
    UPSOUTH,
    UPNORTHEAST,
    UPNORTHWEST,
    UPSOUTHEAST,
    UPSOUTHWEST,
    DOWNNORTH,
    DOWNEAST,
    DOWNWEST,
    DOWNSOUTH,
    DOWNNORTHEAST,
    DOWNNORTHWEST,
    DOWNSOUTHEAST,
    DOWNSOUTHWEST;

    public static final Map<BaseDirection, BaseDirection> opposites = ImmutableMap.<BaseDirection, BaseDirection>builder()
            .put(DOWN, UP)
            .put(UP, DOWN)
            .put(NORTH, SOUTH)
            .put(SOUTH, NORTH)
            .put(EAST, WEST)
            .put(WEST, EAST)
            .put(NORTHEAST, SOUTHWEST)
            .put(SOUTHWEST, NORTHEAST)
            .put(NORTHWEST, SOUTHEAST)
            .put(SOUTHEAST, NORTHWEST)
            .put(UPNORTH, DOWNSOUTH)
            .put(DOWNSOUTH, UPNORTH)
            .put(UPEAST, DOWNWEST)
            .put(DOWNWEST, UPEAST)
            .put(UPWEST, DOWNEAST)
            .put(DOWNEAST, UPWEST)
            .put(UPSOUTH, DOWNNORTH)
            .put(DOWNNORTH, UPSOUTH)
            .put(UPNORTHEAST, DOWNSOUTHWEST)
            .put(DOWNSOUTHWEST, UPNORTHEAST)
            .put(UPNORTHWEST, DOWNSOUTHEAST)
            .put(DOWNSOUTHEAST, UPNORTHWEST)
            .put(UPSOUTHEAST, DOWNNORTHWEST)
            .put(DOWNNORTHWEST, UPSOUTHEAST)
            .put(UPSOUTHWEST, DOWNNORTHEAST)
            .put(DOWNNORTHEAST, UPSOUTHWEST)
            .build();

    public static final Map<BaseDirection, Set<Direction>> neighbours = ImmutableMap.<BaseDirection, Set<Direction>>builder()
            .put(DOWN, Set.of(DOWNNORTH, DOWNEAST, DOWNWEST, DOWNSOUTH, DOWNNORTHEAST, DOWNNORTHWEST, DOWNSOUTHEAST, DOWNSOUTHWEST))
            .put(UP, Set.of(UPNORTH, UPEAST, UPWEST, UPSOUTH, UPNORTHEAST, UPNORTHWEST, UPSOUTHEAST, UPSOUTHWEST))
            .put(NORTH, Set.of(NORTHEAST, NORTHWEST, DOWNNORTH, UPNORTH, DOWNNORTHEAST, DOWNNORTHWEST, UPNORTHWEST, UPNORTHEAST))
            .put(SOUTH, Set.of(SOUTHEAST, SOUTHWEST, DOWNSOUTH, UPSOUTH, DOWNSOUTHEAST, DOWNSOUTHWEST, UPSOUTHWEST, UPSOUTHEAST))
            .put(EAST, Set.of(NORTHEAST, SOUTHEAST, DOWNNORTHEAST, DOWNSOUTHEAST, UPSOUTHEAST, UPNORTHWEST, DOWNEAST, UPEAST))
            .put(WEST, Set.of(NORTHWEST, SOUTHWEST, DOWNNORTHWEST, DOWNSOUTHWEST, UPSOUTHWEST, UPNORTHWEST, DOWNWEST, UPWEST))
            .put(NORTHEAST, Set.of(NORTH, EAST, UPNORTHEAST, DOWNNORTHEAST))
            .put(NORTHWEST, Set.of(NORTH, WEST, UPNORTHWEST, DOWNNORTHWEST))
            .put(SOUTHEAST, Set.of(SOUTH, EAST, UPSOUTHEAST, DOWNSOUTHEAST))
            .put(SOUTHWEST, Set.of(SOUTH, WEST, UPSOUTHWEST, DOWNSOUTHWEST))
            .put(UPNORTH, Set.of(UP, NORTH, UPNORTHWEST, UPNORTHEAST))
            .put(UPSOUTH, Set.of(UP, SOUTH, UPSOUTHWEST, UPSOUTHEAST))
            .put(UPEAST, Set.of(UP, EAST, UPNORTHEAST, UPSOUTHEAST))
            .put(UPWEST, Set.of(UP, WEST, UPNORTHWEST, UPSOUTHWEST))
            .put(DOWNNORTH, Set.of(DOWN, NORTH, DOWNNORTHWEST, DOWNNORTHEAST))
            .put(DOWNSOUTH, Set.of(DOWN, SOUTH, DOWNSOUTHWEST, DOWNSOUTHEAST))
            .put(DOWNEAST, Set.of(DOWN, EAST, DOWNNORTHEAST, DOWNSOUTHEAST))
            .put(DOWNWEST, Set.of(DOWN, WEST, DOWNNORTHWEST, DOWNSOUTHWEST))

            .put(UPNORTHEAST, Set.of(UP, NORTH, EAST))
            .put(DOWNSOUTHWEST, Set.of(DOWN, SOUTH, WEST))
            .put(UPNORTHWEST, Set.of(UP, NORTH, WEST))
            .put(DOWNSOUTHEAST, Set.of(DOWN, SOUTH, EAST))
            .put(UPSOUTHEAST, Set.of(UP, SOUTH, EAST))
            .put(DOWNNORTHWEST, Set.of(DOWN, NORTH, WEST))
            .put(UPSOUTHWEST, Set.of(UP, SOUTH, WEST))
            .put(DOWNNORTHEAST, Set.of(DOWN, NORTH, EAST))
            .build();

    @Override
    public Set<Direction> getNeighbours() {
        return neighbours.get(this);
    }

    @Override
    public Direction getOpposite() {
        return opposites.get(this);
    }

    @Override
    public List<Direction> getAll() {
        return Arrays.asList(BaseDirection.values());
    }

    @Override
    public String toString() {
        return this.name();
    }
}
