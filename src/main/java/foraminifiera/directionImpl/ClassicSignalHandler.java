package foraminifiera.directionImpl;

import configuration.Parameters;
import simulation.model.Cell;
import simulation.Direction;
import simulation.NeighbourSignalHandler;
import simulation.Signal;
import foraminifiera.model.ForaminiferaCell;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ClassicSignalHandler implements NeighbourSignalHandler {

    @Override
    public List<Signal> getSignalsToPropagate(Cell cell) {
        if (cell.isBlocked()) {
            return Collections.emptyList();
        }
        Map<Direction, Signal> newDirectionToSignals = ((ForaminiferaCell) cell).propagateSignals();
        Map<Direction, Signal> incomingSignals = cell.getSignals()
                .stream()
                .collect(Collectors.toMap(Signal::getDirection, Function.identity()));
        HashMap<Direction, Double> outcomingSignals = new HashMap<>();
        incomingSignals.forEach((direction, signal) -> {
            Set<Direction> directionNeighbours = direction.getNeighbours();
            outcomingSignals.put(direction, signal.getValue());
            directionNeighbours.forEach(neighbour -> outcomingSignals.put(neighbour, signal.getValue() * Parameters.NEIGHBOUR_MULTIPLIER));
        });
        return newDirectionToSignals.entrySet()
                .stream()
                .map(directionToSignal -> {
                    double value = outcomingSignals.getOrDefault(directionToSignal.getKey(), 0.0) * Parameters.SUPPRESSION_FACTOR + directionToSignal.getValue().getValue() * Parameters.ATTENUATION_FACTOR;
                    return new Signal(directionToSignal.getKey(), value);
                })
                .collect(Collectors.toList());
    }
}
