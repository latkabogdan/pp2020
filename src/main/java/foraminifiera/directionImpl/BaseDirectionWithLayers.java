package foraminifiera.directionImpl;

import com.google.common.collect.ImmutableMap;
import simulation.Direction;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public enum BaseDirectionWithLayers implements Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    TOPLEFT,
    TOPRIGHT,
    DOWNLEFT,
    DOWNRIGHT,
    UPPER,
    LOWER;

    public static final Map<BaseDirectionWithLayers, BaseDirectionWithLayers> opposites = ImmutableMap.<BaseDirectionWithLayers, BaseDirectionWithLayers>builder()
            .put(DOWN, UP)
            .put(UP, DOWN)
            .put(RIGHT, LEFT)
            .put(LEFT, RIGHT)
            .put(DOWNRIGHT, TOPLEFT)
            .put(DOWNLEFT, TOPRIGHT)
            .put(TOPRIGHT, DOWNLEFT)
            .put(TOPLEFT, DOWNRIGHT)
            .put(UPPER, LOWER)
            .build();

    public static final Map<BaseDirectionWithLayers, Set<Direction>> neighbours = ImmutableMap.<BaseDirectionWithLayers, Set<Direction>>builder()
            .put(DOWN, Set.of(DOWNLEFT, DOWNRIGHT, UPPER, LOWER))
            .put(UP, Set.of(TOPLEFT, TOPRIGHT, UPPER, LOWER))
            .put(RIGHT, Set.of(TOPRIGHT, DOWNRIGHT, UPPER, LOWER))
            .put(LEFT, Set.of(TOPLEFT, DOWNLEFT, UPPER, LOWER))
            .put(DOWNRIGHT, Set.of(RIGHT, DOWN, UPPER, LOWER))
            .put(DOWNLEFT, Set.of(LEFT, DOWN, UPPER, LOWER))
            .put(TOPRIGHT, Set.of(UP, RIGHT, UPPER, LOWER))
            .put(TOPLEFT, Set.of(UP, LEFT, UPPER, LOWER))
            .put(UPPER, Set.of(LEFT, DOWN, UP, RIGHT, DOWNLEFT, DOWNRIGHT, TOPLEFT, TOPRIGHT))
            .put(LOWER, Set.of(LEFT, DOWN, UP, RIGHT, DOWNLEFT, DOWNRIGHT, TOPLEFT, TOPRIGHT))
            .build();

    @Override
    public Set<Direction> getNeighbours() {
        return neighbours.get(this);
    }

    @Override
    public Direction getOpposite() {
        return opposites.get(this);
    }

    @Override
    public List<Direction> getAll() {
        return Arrays.asList(BaseDirectionWithLayers.values());
    }

    @Override
    public String toString() {
        return this.name();
    }
}
