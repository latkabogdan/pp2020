package foraminifiera.graphfactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import configuration.Parameters;
import foraminifiera.ForaminiferaMetrics;
import foraminifiera.directionImpl.BaseDirection;
import org.apache.commons.lang3.tuple.Pair;
import simulation.model.BlockedCell;
import simulation.model.Cell;
import foraminifiera.model.ForaminiferaCell;
import simulation.Direction;
import simulation.GraphSupplier;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class SeaBottomFactory implements GraphSupplier {
    private static Long idCounter = 0L;
    private static final Long LAYER_SIZE = Parameters.WIDTH * Parameters.HEIGHT;
    private static final Map<Direction, Pair<Function<Long, Long>, Predicate<Long>>> DIRECTION_TO_GETTER = ImmutableMap.<Direction, Pair<Function<Long, Long>, Predicate<Long>>>builder()
            .put(BaseDirection.UP, Pair.of(val -> val - LAYER_SIZE, val -> val/LAYER_SIZE != 0))
            .put(BaseDirection.DOWN, Pair.of(val -> val + LAYER_SIZE, val -> val/LAYER_SIZE != Parameters.DEPTH-1))
            .put(BaseDirection.EAST, Pair.of(val -> val + 1, val -> val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.WEST, Pair.of(val -> val - 1, val -> val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.NORTH, Pair.of(val -> val - Parameters.WIDTH, val -> val%LAYER_SIZE/Parameters.WIDTH != 0))
            .put(BaseDirection.SOUTH, Pair.of(val -> val + Parameters.WIDTH, val -> val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1))
            .put(BaseDirection.NORTHEAST, Pair.of(val -> val - Parameters.WIDTH + 1, val -> val%LAYER_SIZE/Parameters.WIDTH != 0 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.NORTHWEST, Pair.of(val -> val - Parameters.WIDTH - 1, val -> val%LAYER_SIZE/Parameters.WIDTH != 0 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.SOUTHEAST, Pair.of(val -> val + Parameters.WIDTH + 1, val -> val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.SOUTHWEST, Pair.of(val -> val + Parameters.WIDTH - 1, val -> val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.UPEAST, Pair.of(val -> val - LAYER_SIZE + 1, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.UPWEST, Pair.of(val -> val - LAYER_SIZE - 1, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.UPNORTH, Pair.of(val -> val - LAYER_SIZE - Parameters.WIDTH, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE/Parameters.WIDTH != 0))
            .put(BaseDirection.UPSOUTH, Pair.of(val -> val - LAYER_SIZE + Parameters.WIDTH, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1))
            .put(BaseDirection.DOWNEAST, Pair.of(val -> val + LAYER_SIZE + 1, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.DOWNWEST, Pair.of(val -> val + LAYER_SIZE - 1, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.DOWNNORTH, Pair.of(val -> val + LAYER_SIZE - Parameters.WIDTH, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE/Parameters.WIDTH != 0))
            .put(BaseDirection.DOWNSOUTH, Pair.of(val -> val + LAYER_SIZE + Parameters.WIDTH, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1))
            .put(BaseDirection.UPSOUTHWEST, Pair.of(val -> val - LAYER_SIZE + Parameters.WIDTH - 1, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.UPSOUTHEAST, Pair.of(val -> val - LAYER_SIZE + Parameters.WIDTH + 1, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.UPNORTHEAST, Pair.of(val -> val - LAYER_SIZE - Parameters.WIDTH + 1, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE/Parameters.WIDTH != 0 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.UPNORTHWEST, Pair.of(val -> val - LAYER_SIZE - Parameters.WIDTH - 1, val -> val/LAYER_SIZE != 0 && val%LAYER_SIZE/Parameters.WIDTH != 0 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.DOWNSOUTHWEST, Pair.of(val -> val + LAYER_SIZE + Parameters.WIDTH - 1, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .put(BaseDirection.DOWNSOUTHEAST, Pair.of(val -> val + LAYER_SIZE + Parameters.WIDTH + 1, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE/Parameters.WIDTH != Parameters.HEIGHT-1 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.DOWNNORTHEAST, Pair.of(val -> val + LAYER_SIZE - Parameters.WIDTH + 1, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE/Parameters.WIDTH != 0 && val%LAYER_SIZE%Parameters.WIDTH != Parameters.WIDTH-1))
            .put(BaseDirection.DOWNNORTHWEST, Pair.of(val -> val + LAYER_SIZE - Parameters.WIDTH - 1, val -> val/LAYER_SIZE != Parameters.DEPTH-1 && val%LAYER_SIZE/Parameters.WIDTH != 0 && val%LAYER_SIZE%Parameters.WIDTH != 0))
            .build();

    public SeaBottomFactory() {
    }

    public List< Cell> getGraph() {
        List<Cell> graph = getGraph1();
        List<Cell> foraminiferaCells = formBottom(graph);
        ForaminiferaInitializer foraminiferaInitializer = new ForaminiferaInitializer();
        Pair<List<Cell>, ForaminiferaMetrics> init = foraminiferaInitializer.init(foraminiferaCells);  //metryki

        //dodać logowanie metryk
        return init.getKey();
    }

    private List<Cell> formBottom(List<Cell> graph) {
        Map<Long, Cell> cellsById = graph.stream()
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
        Map<Long, List<Cell>> cellsByLayer = graph.stream()
                .collect(Collectors.groupingBy(cell -> cell.getId() % Parameters.DEPTH));
        List<Cell> lastLayer = cellsByLayer.get(Parameters.DEPTH - 1);
        addBlockade(cellsById, lastLayer);
        //List<Cell> beforeLastLayer = cellsByLayer.get(Parameters.HEIGHT - 2);
        //List<Cell> halfLastLayer = lastLayer.subList(NumberUtils.INTEGER_ZERO, lastLayer.size()/2 - 1);
        //List<Cell> halfBeforeLastLayer = beforeLastLayer.subList(NumberUtils.INTEGER_ZERO, beforeLastLayer.size()/2 - 1);
        //halfLastLayer.forEach(cell -> blockCell(cellsById, cell));
        //halfBeforeLastLayer.forEach(cell -> blockCell(cellsById, cell));
        ThreadLocalRandom.current().ints(4, 0, (int) (LAYER_SIZE-1)).asLongStream()
                .forEach(num -> {
                    Cell cell = lastLayer.get((int) num);
                    removeGroup(cellsById, cell, 2, 4);
                });
        ThreadLocalRandom.current().ints(LAYER_SIZE, (int) (graph.size() - 2*LAYER_SIZE), graph.size()-1).asLongStream()
                .forEach(num -> blockCell(cellsById, cellsById.get(num)));
        return new LinkedList<>(cellsById.values());
    }

    private void addBlockade(Map<Long, Cell> cellsById, List<Cell> lastLayer) {
        long numberOfRow = Parameters.WIDTH / 3;
        lastLayer.stream()
                .filter(cell -> cell.getId()%Parameters.WIDTH >= numberOfRow - 1 && cell.getId()%Parameters.WIDTH <= numberOfRow + 1)
                .forEach(cell -> {
                    Cell copy = cell;
                    for(int i = 0; i < Parameters.HEIGHT - 3; i++) {
                        Optional<Long> upperNeighbour = Optional.ofNullable(copy.getNeighboursByDirection().get(BaseDirection.UP));
                        blockCell(cellsById, copy);
                        if(upperNeighbour.isEmpty()) {
                            break;
                        } else {
                            copy = cellsById.get(upperNeighbour.get());
                        }
                    }
                });
    }

    private void removeGroup(Map<Long, Cell> cells, Cell cell, Integer size, Integer depth) {
        List<Long> neighbours = new LinkedList<>(cell.getNeighboursByDirection().values());
        Random random = new Random();
        if(neighbours.isEmpty()) {
            return;
        }
        List<Long> selectedNeighbours = random.ints(size, 0, neighbours.size())
                .mapToObj(neighbours::get)
                .collect(Collectors.toList());
        if(depth != 0) {
            selectedNeighbours.stream()
                    .map(cells::get)
                    .filter(c -> !Objects.isNull(c))
                    .forEach(c -> removeGroup(cells, c, size, depth - 1));
        }
        selectedNeighbours.forEach(c -> blockCell(cells, cells.get(c)));
    }

    private void blockCell(Map<Long, Cell> cells, Cell cell) {
        cells.replace(cell.getId(), new BlockedCell(cell));
    }

    private static List<Cell> getGraph1() {
        Map<Long, Cell> cells = LongStream.range(0, Parameters.WIDTH * Parameters.HEIGHT * Parameters.DEPTH)
                .mapToObj(number -> new ForaminiferaCell(idCounter++))
                .collect(Collectors.toMap(Cell::getId, Function.identity()));
        LongStream.range(0, Parameters.WIDTH * Parameters.HEIGHT * Parameters.DEPTH)
                .forEach(index -> cells.get(index).addNeighbours(getNeighbours(index)));
        return new LinkedList<>(cells.values());
    }

    private static Map<Direction, Long> getNeighbours(Long index) {
        Map<Direction, Pair<Function<Long, Long>, Predicate<Long>>> filteredNeighbours = Maps.filterValues(DIRECTION_TO_GETTER, val -> val.getValue().test(index));
        Map<Direction, Long> directionToNeighbours = Maps.transformValues(filteredNeighbours, pair -> pair.getKey().apply(index));
        return Maps.filterValues(directionToNeighbours, SeaBottomFactory::isCorrect);
    }

    private static boolean isCorrect(long index) {
        return index >= 0 && index < Parameters.HEIGHT * Parameters.WIDTH * Parameters.DEPTH;
    }
}
