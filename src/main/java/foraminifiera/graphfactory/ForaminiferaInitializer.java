package foraminifiera.graphfactory;

import configuration.Parameters;
import foraminifiera.ForaminiferaMetrics;
import org.apache.commons.lang3.tuple.Pair;
import simulation.model.BlockedCell;
import simulation.model.Cell;
import foraminifiera.model.ForaminiferaCell;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ForaminiferaInitializer {

    public Pair<List<Cell>, ForaminiferaMetrics> init(List<Cell> cells) {
        Random random = new Random();
        Double algaeCount = 0.0;
        long foraminiferaCount = 0L;
        List<Cell> newCells = new LinkedList<>();
        for (Cell cell: cells) {
            if(cell instanceof BlockedCell) {
                newCells.add(cell);
                continue;
            }
            ForaminiferaCell foraminiferaCell = (ForaminiferaCell) cell;
            if(random.nextDouble() < Parameters.FORAMINIFERA_SPAWN_CHANCE) {
                int numberOfForaminiferas = random.nextInt(Parameters.FORAMINIFERA_MAX_START_SPAWN);
                for(int i = 0; i < numberOfForaminiferas; i++) {
                    foraminiferaCell.addForaminifera(Parameters.FORAMINIFERA_START_ENERGY);
                    foraminiferaCount++;
                }
            }
            double algaeEnergy = random.nextDouble() * Parameters.ALGAE_MAX_START_VALUE;
            foraminiferaCell.setAlgae(algaeEnergy);
            algaeCount += algaeEnergy;
            newCells.add(foraminiferaCell);
        }

        return Pair.of(newCells, new ForaminiferaMetrics(algaeCount, foraminiferaCount));
    }
}
